Chained Property Files (CPF)
============================

*CPF* is a simple library for reading property files (key,value) from
formatted text files. Compared to standard Java property files, CPF
provides a way to *chain* one property file to another. The feature is
similar to how inheritance in OO allows a subclass to override aspects
of its superclass, a CPF file may override a property from a chained
CPF.

The purpose of this mechanism is to group core configuration in a
single or small set of 'base' CPF files, and then write short CPF's
that override a small set of selected properties; to avoid a lot of
duplication.

Example: A configuration of 30 parameters only differ in two of them
for the production and the testing variant of a program. You then
create a `root.cpf` with the proper values for the 30 parameters; next
create a `production.cpf` that just state that is chained to the
root.cpf and set values for the two special parameters; and similar
for a `testing.cpf`.

License is [Apache 2](LICENSE).

Author: *Henrik Bærbak Christensen / Aarhus University / www.baerbak.com*.

I only need the CPF Library, what is the dependency?
-----

The CPF library is available in MavenCentral:

Get it using Gradle:

    dependencies {
      implementation group: 'com.baerbak.maven', name: 'cpf', version: '3.0.3'
    }


How to include in my application
---

Import it using

    import com.baerbak.cpf.*;

You just provide an instance of
`ChainedPropertyResourceFileReaderStrategy` with a named CPF file, and
use that instance as a getter on values read from the CPF.

    String cpfFile = "testcases/base.cpf";
    
    ChainedPropertyResourceFileReaderStrategy cpfReader = new
        ChainedPropertyResourceFileReaderStrategy(cpfFile);
    
    assertThat(cpfReader.getValue("SKYCAVE_APPSERVER"), 
      is("localhost:37123"));
      
This will pass if the CPF file `testcases/base.cpf` contains

    # === Dummy config of the server IP endpoint
    SKYCAVE_APPSERVER =  localhost:37123      

CPF file format and chaining
----------------------------

A CPF is a file, closely similar to Java property files, which allows
(key,value) pairs to be defined in files which are read at runtime
using this reader. A CPF, however, allows one file to chain (include)
another so more complex property sets can be defined in a modular way.

The format of any CPF is a UTF-8 encoded text files consisting of one of
three types of lines:

  * COMMENT: Any empty line or line starting with `#`
  * PROPERTY: A line of format `key = value`
  * CHAIN: A line of format `< (filename)` (See note on filenames below)

When a CHAIN line is read in a CPF file, the named CPF file is read
*before* proceeding to the following lines. Any PROPERTY line assigns
resulting (key,value) pair in the order they are read, allowing
overwriting properties easily simply by restating the property after
the chain line in the file. Typically, you state the chain line at the
top of the CPF file, and then modify/override the values of selected
keys below.

### Example:

`base.cpf` contains

    prop1 = Mikkel
    prop2 = Magnus

while `layer1.cpf` contains

    < base.cpf
    prop1 = Mathilde

Constructing a ChainedPropertyResourceFileReaderStrategy, cpf, on
`layer1.cpf` will result in both files being read, and two properties
that have the following values


    cpf.getValue("prop1") == "Mathilde";
    cpf.getValue("prop2") == "Magnus";

Thus CPFs are ideal for creating base configurations with default
properties defined, while small changes can easily be made just
by making a CPF that chains to the base configuration.

Note: Only filenames that strictly contain alphanumeric and dash
are accepted!

Where should I put my CPF files?
---

Version 2 of this library reads CPF files (both the root and chained
ones) using the Java class loader and thus respects e.g. Gradle's
convention for organizing resource file. E.g. if you put two CPFs into
the following folders

    src/main/resources/cpf/base.cpf
    src/main/resources/cpf/layer.cpf

you must refer to them as `cpf/base.cpf` and `cpf/layer.cpf` respectively.

    ChainedPropertyResourceFileReaderStrategy cpfReader = new
        ChainedPropertyResourceFileReaderStrategy("cpf/layer.cpf");
        
Also, the chain line must be formatted for layer1 to chain into base:

    < cpf/base.cpf
    
The reading *starts* by trying to read the cpf file as a resource file
(that is from a /resources folder or from within a Java jar file) and
if not found it will try to read from the local file system (rooted in
the current directory in which gradle or java -jar is started).
  
Using the PropertyBuilder to tweak CPF contents (Version 3)
----

Version 3 of the this library introduces a Bloch builder to allow
overwriting key-value pairs read from a CPF file. While it is
encouraged to keep configurations in the environment/CPF files
the builder here allows making changes at runtime/execution time. This
is especially interesting in test situations where for instance
the TestContainers framework will assign random port numbers to
services at runtime which conflicts with the static nature of CPF
files.

To use the builder, you create a `PropertyBuilder()`, invoke methods
on it, and finally call its `build()` method.

    int randomPort = (random value)
    PropertyReaderStrategy cpfReader = new
            PropertyBuilder()
            .withCPFResource("testcases/base.cpf")
            .overwrite(Config.SKYCAVE_APPSERVER, 
                       "localhost:" + randomPort)
            .build();

Here the resulting cpfReader will have all values as defined in
CPF file 'base.cpf' except the SKYCAVE_APPSERVER is assigned
a new value.

Legacy
---

Version 1 reads CPF files throught the file system.

The deprecated old system is still available

    File cpfFile = new File("build/resources/test/testcases/base.cpf");
    
    ChainedPropertyFileReaderStrategy cpfReader = new
        ChainedPropertyFileReaderStrategy(cpfFile);

How to understand the code
---

Review the single learning test in the /test source folder.

Manual testing of CPF loading
---

A simple demonstration has been included for manual testing of

  * Reading a simple CPF file in the resource folder `cpf/`
  * Reading a chained CPF file in the resource folder
  * Reading an 'external' CPF file (in the local folder).
  
To execute using gradle:

    ./gradlew demo -Pcpf=cpf/base.cpf
    ./gradlew demo -Pcpf=cpf/layer.cpf
    ./gradlew demo -Pcpf=external.cpf
    
To execute using a Jar packed

    ./gradlew jar
    java -jar build/libs/demo.jar cpf/base.cpf
    java -jar build/libs/demo.jar cpf/layer.cpf
    java -jar build/libs/demo.jar external.cpf
    
Note how layer.cpf and external.cpf overwrites the SKYCAVE_APPSERVER
property.

Versions
--------

  v 3.0.3: JDK11 (major version 55) with 'module-info'. Require gradle 7+ to build.
           Migrated to 'maven-publish' plugin.
  
  v 3.0.2: Downcasted to JDK8 (major version 52) of library
  
  v 3.0.1: Migrated to Maven Central, update build syntax




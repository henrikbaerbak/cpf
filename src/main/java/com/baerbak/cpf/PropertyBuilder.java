package com.baerbak.cpf;


import java.util.HashMap;
import java.util.Map;

/** A Bloch Builder ("Effective Java") to build a
 * property reader from a CPF resource while allowing
 * to add/overwrite keys to new values.
 *
 * Example:
 *
 *     PropertyReaderStrategy cpfReader = new
 *             PropertyBuilder()
 *             .withCPFResource("testcases/base.cpf")
 *             .add("NEW_KEY", "Findus")
 *             .overwrite(Config.SKYCAVE_APPSERVER, "baerbak.cs.au.dk:7777")
 *             .build();
 *
 * @author Henrik Bærbak Christensen, Aarhus University
 */
public class PropertyBuilder {
  private String cpfResourceFileName;
  private Map<String, String> propertyMap;

  /**
   * Construct an empty builder
   */
  public PropertyBuilder() {
    propertyMap = new HashMap<>();
    cpfResourceFileName = null;
  }

  /**
   * Construct the property reader strategy based
   * upon the contents of the current builder
   * @return an initialized property reader strategy
   */
  public PropertyReaderStrategy build() {
    BasePropertyReaderStrategy prs;
    if (cpfResourceFileName == null) {
      prs = new BasePropertyReaderStrategy();
    } else {
      prs = new ChainedPropertyResourceFileReaderStrategy(cpfResourceFileName);
    }
    propertyMap.keySet()
            .stream()
            .forEach( key -> { prs.setValue(key, propertyMap.get(key)); } );
    return prs;
  }

  /** Seed the property reader with a given CPF file
   *
   * @param cpfResourceFileName name of the class path resource
   * @return a builder
   */
  public PropertyBuilder withCPFResource(String cpfResourceFileName) {
    if (this.cpfResourceFileName != null) {
      throw new CPFConfigurationException("A builder can only accept a SINGLE CPF resource. "
      + "This builder is already set with CPF resource: "
      +this.cpfResourceFileName + ".");
    }
    this.cpfResourceFileName = cpfResourceFileName;
    return this;
  }

  /** Overwrite the contents of a key  with a new value
   *
   * @param key the key
   * @param value the value
   * @return a builder
   */
  public PropertyBuilder overwrite(String key, String value) {
    this.propertyMap.put(key, value);
    return this;
  }

  /** Add the contents of a key  with a new value
   *
   * @param key the key
   * @param value the value
   * @return a builder
   */
  public PropertyBuilder add(String key, String value) {
    this.propertyMap.put(key, value);
    return this;
  }
}

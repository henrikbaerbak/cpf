package com.baerbak.cpf;

/**
 * @author Henrik Bærbak Christensen, Aarhus University
 */
public class CPFConfigurationException extends RuntimeException {
  private static final long serialVersionUID = -2747237182710093604L;

  public CPFConfigurationException(String reason) {
    super(reason);
  }
}

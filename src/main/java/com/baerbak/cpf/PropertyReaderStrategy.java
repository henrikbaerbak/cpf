/*
 * Copyright (C) 2018 Henrik Bærbak Christensen, baerbak.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baerbak.cpf;

/**
 * Strategy (FRS, p. 130) for accessing global properties set for
 * a project. For production, use the implementation that reads
 * chained property files.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University.
 * 
 */
public interface PropertyReaderStrategy {

  /**
   * Read the value of a property of the given name.
   * 
   * @param key
   *          name of the property whose value must be read
   * @return the value of the property
   */
  String getValue(String key);
}

package com.baerbak.cpf;

import java.util.HashMap;
import java.util.Map;

/** Baseclass only holding the underlying datastructure.
 *
 * @author Henrik Bærbak Christensen, Aarhus University
 */
public class BasePropertyReaderStrategy implements PropertyReaderStrategy {
  // Base datastructure, mapping keys to values
  private Map<String, String> propertyMap;

  public BasePropertyReaderStrategy() {
    propertyMap = new HashMap<>();
  }

  @Override
  public String getValue(String key) {
    return propertyMap.get(key);
  }

  protected void setValue(String key, String value) {
    propertyMap.put(key,value);
  }

}

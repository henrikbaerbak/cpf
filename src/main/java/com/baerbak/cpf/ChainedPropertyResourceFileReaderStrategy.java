package com.baerbak.cpf;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A configuration reader strategy based upon Chained Property Files (CPFs)
 * reading them as resource files (through the Java classpath).
 *
 * <p>
 * A CPF is a file, closely similar to Java property files, which allows
 * (key,value) pairs to be defined in files which are read at runtime using this
 * reader. A CPF, however, allows one file to chain (include) another so more complex
 * property sets can be defined in a modular way.
 * <p>
 * The format of any CPF is a UTF-8 encoded text files consisting of one of
 * three types of lines:
 * <ol>
 * <li>COMMENT: Any empty line or line starting with #
 * <li>PROPERTY: A line of format 'key = value'
 * <li>CHAIN: A line of format '&lt; cpf-filename' (See note on filenames below)
 * </ol>
 * <p>
 * When a CHAIN line is read in a CPF file, the named CPF file is read before
 * proceeding to the following lines. Any PROPERTY line assigns resulting
 * (key,value) pair in the order they are read, allowing overwriting
 * properties easily.
 *
 * <p>
 * Example:
 * <p>
 * base.cpf contains
 * <pre>
 * prop1 = Mikkel
 * prop2 = Magnus
 * </pre>
 * while layer1.cpf contains
 * <pre>
 * &lt; base.cpf
 * prop1 = Mathilde
 * </pre>
 * Constructing a ChainedPropertyFileReaderStrategy, cpf, on 'layer1.cpf'
 * will result in two properties with the following values
 *
 * <br>
 * cpf.getProp("prop1") == "Mathilde";
 * <br>
 * cpf.getProp("prop2") == "Magnus";
 *
 * <p>
 * Thus CPFs are ideal for creating base configurations with default
 * properties defined, while small changes can easily be made just
 * by making a CPF that chains to the base configuration.
 * <p>
 * Note: Only filenames that strictly contain alphanumeric and dash
 * are accepted!
 * <p>
 * When CHAINING the filename may (should) contain paths relative to the
 * Java classpath. For instance, in Gradle's standard resource hierarchy
 * you can put a base CPF files in folder
 * <br>
 * src/main/resources/cpf/base.cpf
 * <br>
 * which is equal to CPF file 'cpf/base.cpf' when chained from another
 * file. This will work correctly also if you package CPF's into jar
 * files etc.
 *
 * @author Henrik Baerbak Christensen, Aarhus University.
 */

public class ChainedPropertyResourceFileReaderStrategy
        extends BasePropertyReaderStrategy {

  // Match the '< /opt/weather-service.cpf' chain input line
  private static final String chainLineRegexp = "^<\\s*([\\w-\\./]+)$";
  private static final Pattern chainLinePattern = Pattern.compile(chainLineRegexp);

  // Match 'key=value' with white space around the =
  private static final String propertyAssignmentRegexp = "^\\s*(\\w+)\\s*=\\s*(\\S+)\\s*$";
  private static final Pattern propertyPattern = Pattern.compile(propertyAssignmentRegexp);

  // Match either '# text' or empty line, nothing else
  private static final String commentLineRegexp = "^(\\s*|#.*)$";
  private static final Pattern commentPattern = Pattern.compile(commentLineRegexp);

  /**
   * Read the CPF file (and any chained ones) and update the internal
   * properties.
   *
   * @param cpfFileName the name of the CPF file to read
   */

  public ChainedPropertyResourceFileReaderStrategy(String cpfFileName) {
    super();
    recurseAndSetProperties(cpfFileName);
  }

  public ChainedPropertyResourceFileReaderStrategy() {
    super();
  }

  /**
   * Read the given file (and recursively any chained from it) and set the property map
   * accordingly
   *
   * @param cpfFileName
   */
  protected void recurseAndSetProperties(String cpfFileName) {
    List<String> ancestorFileName = new ArrayList<>();
    recurseAndSetProperties(cpfFileName, ancestorFileName);
  }

  /**
   * Read the given file (and recursively any chained from it) and set the property map
   * accordingly while keeping a list of previously visited CPF files, to allow
   * better feedback in case of errors in one of the files in the chain.
   * @param cpfFileName name of the cpf file to read
   * @param ancestorFileName list of all previously read CPF files
   */
  protected void recurseAndSetProperties(String cpfFileName, List<String> ancestorFileName) {
    // Add to ancestor list
    ancestorFileName.add(cpfFileName);

    // Read in all lines from the cpf file
    List<String> contents = readAllLines(cpfFileName, ancestorFileName);

    // And process each line
    int lineCountInThisFile = 0;
    for(String line: contents) {
      Matcher m = chainLinePattern.matcher(line);
      lineCountInThisFile++;
      if (m.find()) {
        String fileName = m.group(1);
        // recurse on that file
        recurseAndSetProperties(fileName, ancestorFileName);
      } else {
        processOneLine(line, cpfFileName, lineCountInThisFile, ancestorFileName);
      }
    }
  }

  private List<String> readAllLines(String cpfFileName, List<String> ancestorFileName) {
    List<String> contents = null;

    // Read in the file
    try {
      contents = null;
      InputStreamReader isr = getInputStreamReaderFor(cpfFileName);

      BufferedReader br = new BufferedReader(isr);
      contents = br.lines().collect(Collectors.toList());
      br.close();

    } catch (IOException e) {
      // Fail fast
      throw new CPFConfigurationException(
              "The CPF file '" + cpfFileName
                      + "' is missing. "
                      + "It is within the chain: "
                      + String.join(", ", ancestorFileName));
    }
    return contents;
  }

  /** Given the name of a cpf file, return a UTF-8 reader
   * instance on it
   * @param cpfFileName name of the cpf file
   * @return an input stream reader on the file
   * @throws FileNotFoundException
   * @throws UnsupportedEncodingException
   */
  private InputStreamReader getInputStreamReaderFor(String cpfFileName)
          throws FileNotFoundException, UnsupportedEncodingException {
    InputStream inputStream = null;

    // Now - there are two possibilities: a) the
    // cpf file is a resource in the class path OR
    // b) it is an absolute file in the file system.
    // We need to support both!

    ClassLoader classLoader = getClass().getClassLoader();
    URL resource = classLoader.getResource(cpfFileName);
    if (resource != null) {
      // it is a) : the file is a resource in a jar file or
      // gradle resource tree
      inputStream = classLoader.getResourceAsStream(cpfFileName);
    } else {
      inputStream = new FileInputStream(cpfFileName);
    }

    return new InputStreamReader(inputStream, "UTF-8");
  }

  /** Process one single line in a CPF file for format check and
   * assignment to the property map.
   * PRECONDITON: the given line is NOT a chain lines ('< ...')
   * @param line the line to process
   * @param cpfFileName the name of the origin cpfFileName
   * @param lineCount the line number of this line in
   *                  this cpfFileName
   * @param ancestorFileName list of all chained cpfs leading
   *                         to this one
   */
  private void processOneLine(String line, String cpfFileName, int lineCount, List<String> ancestorFileName) {
    Matcher m = propertyPattern.matcher(line);
    if (m.find()) {
      String key = m.group(1); String value = m.group(2);
      setValue(key, value);
    } else {
      m = commentPattern.matcher(line);
      if (m.find()) {
        // Ignore comments
      } else {
        m = chainLinePattern.matcher(line);
        if (m.find()) {
          // Ignore chaining lines, they are processed in the
          // reading phase
        } else {
          throw new CPFConfigurationException("At line " + lineCount
                  + " in file '" + cpfFileName
                  + "': The following line does not match the CPF format: "
                  + "'" + line + "'. "
                  + "It is within the chain: "
                  + String.join(", ", ancestorFileName));
        }
      }
    }
  }

  /**
   * Given a resource file name (cpf filename) try to located the
   * input file.
   */
  @Deprecated
  protected File getFileFromFileName(String cpfFileName) {
    // Source: https://www.mkyong.com/java/java-read-a-file-from-resources-folder/
    ClassLoader classLoader = getClass().getClassLoader();
    URL resource = classLoader.getResource(cpfFileName);
    File fileFound = null;
    if (resource != null) {
      String cpfFile = resource.getFile();
      fileFound = new File(cpfFile);

    } else {
      // Try to read it raw from the file system
      fileFound = new File(cpfFileName);
      if (!fileFound.exists())
        throw new CPFConfigurationException("The CPF file '" + cpfFileName + "' is missing.");
    }
    return fileFound;
  }

}

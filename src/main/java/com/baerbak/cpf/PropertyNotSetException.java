/*
 * Copyright (C) 2018 Henrik Bærbak Christensen, baerbak.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baerbak.cpf;

/** Misleading exception name, will be removed in CPF library
 * version 4. Use 'CPFConfigurationException' instead.
 *
 * @author Henrik Baerbak Christensen, Aarhus University.
 *
 */
@Deprecated
public class PropertyNotSetException extends RuntimeException {

  private static final long serialVersionUID = -2747237182710093603L;

  public PropertyNotSetException(String reason) {
    super(reason);
  }

}

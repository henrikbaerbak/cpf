/*
 * Copyright (C) 2018 Henrik Bærbak Christensen, baerbak.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baerbak.cpf;

import java.io.*;

/**
 * A configuration reader strategy based upon Chained Property Files (CPFs).
 * This version 1.0 reader is only maintained for legacy. It reads directly
 * from the file system which match e.g. gradle resource folder hierarchy
 * badly.
 *
 * Use the ChainedPropertyResourceFileReaderStrategy instead and put
 * the CPFs into the java class path.
 *
 * @author Henrik Baerbak Christensen, Aarhus University.
 */

@Deprecated
public class ChainedPropertyFileReaderStrategy extends ChainedPropertyResourceFileReaderStrategy {

  /**
   * Read the CPF file (and any chained ones) and update the internal
   * properties.
   * 
   * @param cpfFile
   *          the name of the CPF file to read
   */
  public ChainedPropertyFileReaderStrategy(File cpfFile) {
    this();
    recurseAndSetProperties(cpfFile.getPath());
  }

  public ChainedPropertyFileReaderStrategy() {
    super();
  }

  @Override
  protected File getFileFromFileName(String cpfFileName) {
    return new File(cpfFileName);
  }

}

package com.baerbak.cpf.main;

import com.baerbak.cpf.ChainedPropertyResourceFileReaderStrategy;

public class DemoCpf {
  public static void main(String[] args) {
    System.out.println("*** CPF reading demonstration ***");
    System.out.println("-- Reading CPF file: " + args[0]);

    String cpfFileName = args[0];

    ChainedPropertyResourceFileReaderStrategy map =
            new ChainedPropertyResourceFileReaderStrategy(cpfFileName);

    System.out.println("SKYCAVE_APPSERVER = " + map.getValue("SKYCAVE_APPSERVER"));
    System.out.println("SKYCAVE_SUBSCRIPTIONSERVER = " + map.getValue("SKYCAVE_SUBSCRIPTIONSERVER"));
  }
}

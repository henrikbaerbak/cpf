/*
 * Copyright (C) 2018 Henrik Bærbak Christensen, baerbak.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baerbak.cpf;

import org.junit.Test;

import java.io.*;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;

/**
 * TDD of the Chained Property File (CPF) reading system utilized in various
 * projects to read in properties.
 *
 * As the CPF system was developed in a teaching project, SkyCave, the
 * test cases shows significant historical traits.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University.
 *
 */
public class TestChainedPropertyFiles {
  @Test
  public void shouldReadCorrectlyFormattedCPFFile() throws IOException {
    String cpfFile = "testcases/base.cpf";
    
    ChainedPropertyResourceFileReaderStrategy cpfReader = new
            ChainedPropertyResourceFileReaderStrategy(cpfFile);
    
    assertThat(cpfFile, is(notNullValue()));
    
    assertThat(cpfReader.getValue(Config.SKYCAVE_APPSERVER), is("localhost:37123"));
    assertThat(cpfReader.getValue(Config.SKYCAVE_CLIENTREQUESTHANDLER_IMPLEMENTATION), 
        is("cloud.cave.doubles.AllTestDoubleClientRequestHandler"));
    
    assertThat(cpfReader.getValue(Config.SKYCAVE_SUBSCRIPTION_IMPLEMENTATION), 
        is("cloud.cave.doubles.TestStubSubscriptionService"));
    assertThat(cpfReader.getValue(Config.SKYCAVE_SUBSCRIPTIONSERVER), 
        is("localhost:42042"));
  }

  @Test
  public void shouldRejectNonCommentLine() throws IOException {
    try {
      new ChainedPropertyResourceFileReaderStrategy("testcases/wrong1.cpf");
      fail("No exception was thrown!");
    } catch (CPFConfigurationException e) {
      assertThat(e.getMessage(), containsString("At line 3 in file "));
      assertThat(e.getMessage(), containsString("wrong1.cpf"));
      assertThat(e.getMessage(), containsString("=== Configure for"));
    }
  }
  
  @Test
  public void shouldRejectLineWithSetInFrontOfProperty() throws IOException {
    try {
      new ChainedPropertyResourceFileReaderStrategy("testcases/wrong2.cpf");
      fail("No exception was thrown!");
    } catch (CPFConfigurationException e) {
      assertThat(e.getMessage(), containsString("At line 4 in file"));
      assertThat(e.getMessage(), containsString("wrong2.cpf"));
      assertThat(e.getMessage(), containsString("set SKYCAVE"));
    }
  }

  @Test
  public void shouldRejectLineWithExportInFrontOfProperty() throws IOException {
    try {
      new ChainedPropertyResourceFileReaderStrategy("testcases/wrong3.cpf");
      fail("No exception was thrown!");
    } catch (CPFConfigurationException e) {
      assertThat(e.getMessage(), containsString("At line 4 in file"));
      assertThat(e.getMessage(), containsString("wrong3.cpf"));
      assertThat(e.getMessage(), containsString("export SKYCAVE"));
    }
  }
  
  @Test
  public void shouldErrOnNonExistingFile() throws IOException {
    try {
      new ChainedPropertyResourceFileReaderStrategy("testcases/nonexisting.cpf");
      fail("No exception was thrown!");
    } catch (CPFConfigurationException e) {
      assertThat(e.getMessage(), containsString("'testcases/nonexisting.cpf' is missing"));
    }
  }

  @Test
  public void shouldReadCorrectlyChainedProperties() throws IOException {
    ChainedPropertyResourceFileReaderStrategy cpfReader = new
            ChainedPropertyResourceFileReaderStrategy("testcases/layer1.cpf");
    
    // Verify that the contents of layer1 has been read
    assertThat(cpfReader.getValue("SKYCAVE_PROPERTY1"), is("wingdings"));
    // Verify that layer1 properties take precedence over chained layers
    assertThat(cpfReader.getValue(Config.SKYCAVE_APPSERVER), is("10.11.85.66:37123"));
    
    // Verify that chained layer properties are defined
    assertThat(cpfReader.getValue(Config.SKYCAVE_SUBSCRIPTION_IMPLEMENTATION), 
        is("cloud.cave.doubles.TestStubSubscriptionService"));
    assertThat(cpfReader.getValue(Config.SKYCAVE_WEATHERSERVER), 
        is("localhost:8281"));
  }
  
  @Test
  public void shouldFailOnBrokenChains() throws IOException {
    try {
      new ChainedPropertyResourceFileReaderStrategy("testcases/layerbroken.cpf");
      fail("No exception was thrown!");
    } catch (CPFConfigurationException e) {
      assertThat(e.getMessage(), containsString("nonexist.cpf' is missing"));
      
    }
  }
  
  @Test
  public void shouldHandleCPFsWithFilenameWithDashInThem() throws IOException {
    // The latest reads TWO cpfs, 
    // < testcases/root-file.cpf
    // < testcases/base.cpf
    // in root-file, the port is 37145 while in base it is 37123
    // Thus 37123 must take precedens as it is read last
    ChainedPropertyResourceFileReaderStrategy cpfReader = new
            ChainedPropertyResourceFileReaderStrategy("testcases/latest.cpf");

    assertThat(cpfReader.getValue(Config.SKYCAVE_APPSERVER), is("localhost:37123"));

    // Verify that the contents of layer1 has been read
    assertThat(cpfReader.getValue("SKYCAVE_PROPERTY1"), is("wingdings"));

  }

  // TDD of the Version 2 CPF reader that reads files in the resource hierarchy,
  // through the classpath.
  @Test
  public void shouldReadCorrectlyFormattedCPFResource() throws IOException {

    ChainedPropertyResourceFileReaderStrategy cpfReader = new
            ChainedPropertyResourceFileReaderStrategy("testcases/base.cpf");

    assertThat(cpfReader.getValue(Config.SKYCAVE_APPSERVER), is("localhost:37123"));
    assertThat(cpfReader.getValue(Config.SKYCAVE_CLIENTREQUESTHANDLER_IMPLEMENTATION),
            is("cloud.cave.doubles.AllTestDoubleClientRequestHandler"));

    assertThat(cpfReader.getValue(Config.SKYCAVE_SUBSCRIPTION_IMPLEMENTATION),
            is("cloud.cave.doubles.TestStubSubscriptionService"));
    assertThat(cpfReader.getValue(Config.SKYCAVE_SUBSCRIPTIONSERVER),
            is("localhost:42042"));
  }

  @Test
  public void shouldReadCorrectlyFormattedCPFResourceChain() throws IOException {

    ChainedPropertyResourceFileReaderStrategy cpfReader = new
            ChainedPropertyResourceFileReaderStrategy("testcases/layer1-resource.cpf");

    // Test chained values are kept
    assertThat(cpfReader.getValue(Config.SKYCAVE_CLIENTREQUESTHANDLER_IMPLEMENTATION),
            is("cloud.cave.doubles.AllTestDoubleClientRequestHandler"));

    // and values are overridden
    assertThat(cpfReader.getValue(Config.SKYCAVE_APPSERVER), is("10.11.85.77:37123"));
    assertThat(cpfReader.getValue("SKYCAVE_PROPERTY1"), is("wingdings2"));
  }

  @Test
  public void shouldRejectLineWithSetInChainedFile() throws IOException {
    try {
      new ChainedPropertyResourceFileReaderStrategy("testcases/wrong-chained2.cpf");
      fail("No exception was thrown!");
    } catch (CPFConfigurationException e) {
      assertThat(e.getMessage(), containsString("At line 5 in file"));
      assertThat(e.getMessage(), containsString("wrong-chained.cpf"));

      assertThat(e.getMessage(), containsString("It is within the chain:"));
      assertThat(e.getMessage(), containsString("wrong-chained2.cpf"));
      assertThat(e.getMessage(), containsString("SET SOME_PROP"));
    }
  }

  @Test
  public void shouldSupportLegayFileReader() throws IOException {
    File cpfFile = new File("build/resources/test/testcases/base.cpf");

    ChainedPropertyResourceFileReaderStrategy cpfReader = new
            ChainedPropertyFileReaderStrategy(cpfFile);

    assertThat(cpfFile, is(notNullValue()));

    assertThat(cpfReader.getValue(Config.SKYCAVE_APPSERVER), is("localhost:37123"));
    assertThat(cpfReader.getValue(Config.SKYCAVE_CLIENTREQUESTHANDLER_IMPLEMENTATION),
            is("cloud.cave.doubles.AllTestDoubleClientRequestHandler"));

    assertThat(cpfReader.getValue(Config.SKYCAVE_SUBSCRIPTION_IMPLEMENTATION),
            is("cloud.cave.doubles.TestStubSubscriptionService"));
    assertThat(cpfReader.getValue(Config.SKYCAVE_SUBSCRIPTIONSERVER),
            is("localhost:42042"));
  }

  // TDD of version 2.2 CPF reader which uses the Bloch Builder pattern
  @Test
  public void shouldSupportBuildingPropertyContents() {
    // Given a PropertyBuilder based upon 'base.cpf'
    PropertyReaderStrategy cpfReader = new
            PropertyBuilder()
            .withCPFResource("testcases/base.cpf")
            .add("NEW_KEY", "Findus")
            .overwrite(Config.SKYCAVE_APPSERVER, "baerbak.cs.au.dk:7777")
            .build();

    // Then it contains the properties expected from base.cpf
    assertThat(cpfReader.getValue(Config.SKYCAVE_SUBSCRIPTION_IMPLEMENTATION),
            is("cloud.cave.doubles.TestStubSubscriptionService"));
    assertThat(cpfReader.getValue(Config.SKYCAVE_SUBSCRIPTIONSERVER),
            is("localhost:42042"));

    // Then APPSERVER was overwritten
    assertThat(cpfReader.getValue(Config.SKYCAVE_APPSERVER), is("baerbak.cs.au.dk:7777"));

    // Then NEW_KEY was defined
    assertThat(cpfReader.getValue("NEW_KEY"), is("Findus"));
  }

  @Test
  public void shouldAllowBuildFromNoBase() {
    // Given a property build with NO base CPF
    PropertyReaderStrategy cpfReader = new
            PropertyBuilder()
            .add("NEW_KEY", "Findus")
            .add(Config.SKYCAVE_APPSERVER, "baerbak.cs.au.dk:7777")
            .build();
    // Then SUBSCRIPTION... is not defined
    assertThat(cpfReader.getValue(Config.SKYCAVE_SUBSCRIPTION_IMPLEMENTATION),
            is(nullValue()));

    // Then APPSERVER and NEW_KEY is
    assertThat(cpfReader.getValue(Config.SKYCAVE_APPSERVER), is("baerbak.cs.au.dk:7777"));
    assertThat(cpfReader.getValue("NEW_KEY"), is("Findus"));
  }

  @Test
  public void shouldNotAllowTwoCPFToBeSet() {
    // Given a builder with two CPF resources given
    try {
      PropertyReaderStrategy cpfReader = new
              PropertyBuilder()
              .withCPFResource("testcases/base.cpf")
              .withCPFResource("testcases/latest.cpf")
              .build();
      fail("A PropertyBuilder shall not accept TWO CPF files.");
    } catch (CPFConfigurationException e) {
      assertThat(e.getMessage(),
              is("A builder can only accept a SINGLE CPF resource. "
                      +"This builder is already set with CPF resource: testcases/base.cpf."));
    }
  }


  @Test
  public void shouldAllowAWSLambdaPaths() {
    // Given a property build with a AWS lambda path
    PropertyReaderStrategy cpfReader = new
            PropertyBuilder()
            .withCPFResource("testcases/aws.cpf")
            .build();
    // Then it can be read
    assertThat(cpfReader.getValue(Config.SKYCAVE_APPSERVER), is("nodexrd2.cw7jbiq3uokf.ap-southeast-2.rds.amazonaws.com:8080"));
  }
}

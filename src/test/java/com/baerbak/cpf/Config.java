/*
 * Copyright (C) 2018 Henrik Bærbak Christensen, baerbak.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baerbak.cpf;

/**
 * Config encapsulates the names of CPF file property keys.
 * For historical reasons, they refer to the mother project
 * which gave rise to the CPF library.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class Config {

  /**
   * Property that must be set to 'name:port' of the endpoint for
   * the application server. In case of a cluster, separate each endpoint with
   * ';'.
   */
  public static final String SKYCAVE_APPSERVER = "SKYCAVE_APPSERVER";

  /**
   * Property that must be set to the 'name:port' of the database
   * server. Separate with ',' / COMMA in case of a cluster.
   */
  public static final String SKYCAVE_DBSERVER = "SKYCAVE_DBSERVER";

  /**
   * Property that must be set to the 'name:port' of the
   * subscription server end point.
   */
  public static final String SKYCAVE_SUBSCRIPTIONSERVER = "SKYCAVE_SUBSCRIPTIONSERVER";

  /**
   * Property that must be set to the 'name:port' of the weather
   * server end point.
   */
  public static final String SKYCAVE_WEATHERSERVER = "SKYCAVE_WEATHERSERVER";

  /**
   * Property that must be set to the fully qualified class name of
   * the class implementing the cave storage interface. This class must be in
   * the classpath and will be loaded at runtime by the ServerFactory.
   */
  public static final String SKYCAVE_CAVESTORAGE_IMPLEMENTATION = "SKYCAVE_CAVESTORAGE_IMPLEMENTATION";

  /**
   * Property that must be set to the fully qualified class name of
   * the class implementing the subscription service interface. This class must
   * be in the classpath and will be loaded at runtime by the ServerFactory.
   */
  public static final String SKYCAVE_SUBSCRIPTION_IMPLEMENTATION = "SKYCAVE_SUBSCRIPTION_IMPLEMENTATION";

  /**
   * Property that must be set to the fully qualified class name of
   * the class implementing the weather service interface. This class must be in
   * the classpath and will be loaded at runtime by the ServerFactory.
   */
  public static final String SKYCAVE_WEATHER_IMPLEMENATION = "SKYCAVE_WEATHER_IMPLEMENTATION";

  /**
   * Property that must be set to the fully qualified class name of
   * the class implementing the weather service interface. This class must be in
   * the classpath and will be loaded at runtime by the ServerFactory.
   */
  public static final String SKYCAVE_SERVERREQUESTHANDLER_IMPLEMENTATION = "SKYCAVE_SERVERREQUESTHANDLER_IMPLEMENTATION"; 

  /**
   * Property that must be set to the fully qualified class name of
   * the class implementing the client request handler interface. This class must be in
   * the classpath and will be loaded at runtime by the ClientFactory.
   */
  public static final String SKYCAVE_CLIENTREQUESTHANDLER_IMPLEMENTATION = "SKYCAVE_CLIENTREQUESTHANDLER_IMPLEMENTATION";

  /**
   * Property key whose value must be a fully qualified class name of the class
   * implementing the PlayerSessionCache. This class must be in the classpath
   * and will be loaded at runtime by the ServerFactory.
   */
  public static final String SKYCAVE_PLAYERSESSIONCACHE_IMPLEMENTATION = "SKYCAVE_PLAYERSESSIONCACHE_IMPLEMENTATION";

  /**
   * Property key whose value defines the hostname/ip of a session cache server.
   */
  public static final String SKYCAVE_PLAYERSESSIONCACHESERVER = "SKYCAVE_PLAYERSESSIONCACHESERVER";

  /**
   * Property key whose value must be a fully qualified class name of the class
   * implementing the Inspector. This class must be in the classpath
   * and will be loaded at runtime by the ServerFactory.
   */
  public static final String SKYCAVE_INSPECTOR_IMPLEMENTATION = "SKYCAVE_INSPECTOR_IMPLEMENTATION";

  /** Property key whoe value defines the hostname/ip of a potential
   * external inspector.
   */
  public static final String SKYCAVE_INSPECTORSERVER = "SKYCAVE_INSPECTORSERVER";
}
